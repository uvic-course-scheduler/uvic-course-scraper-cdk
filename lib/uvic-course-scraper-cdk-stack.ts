import { SubnetType } from "@aws-cdk/aws-ec2";
import { Repository } from "@aws-cdk/aws-ecr";
import { Cluster, ContainerImage, FargatePlatformVersion, Secret } from "@aws-cdk/aws-ecs";
import { ScheduledFargateTask } from "@aws-cdk/aws-ecs-patterns";
import { Schedule } from "@aws-cdk/aws-events";
import { Bucket } from '@aws-cdk/aws-s3';
import { StringParameter } from "@aws-cdk/aws-ssm";
import * as cdk from '@aws-cdk/core';

export class UvicCourseScraperCdkStack extends cdk.Stack {
  constructor(scope: cdk.Construct, id: string, props?: cdk.StackProps) {
    super(scope, id, props);

    // S3 bucket for logging raw scraper data
    const bucket = new Bucket(this, 'scraper-data', {
      removalPolicy: cdk.RemovalPolicy.DESTROY,
      autoDeleteObjects: true
    });

    // ECR repository for scraper docker image
    const repository = new Repository(this, 'scraper-repository', {});

    // ECS cluster on which to run the scraper
    const cluster = new Cluster(this, 'course-scraper-cluster', {});

    // Get access key id and secret key for scraper IAM user
    const keyIdParameter = StringParameter.fromSecureStringParameterAttributes(this, 'course-uploader-access-key-id', { 
      parameterName: 'course-uploader-access-key-id',
      version: 1
    });
    const secretKeyParameter = StringParameter.fromSecureStringParameterAttributes(this, 'course-uploader-secret-key', { 
      parameterName: 'course-uploader-secret-key',
      version: 1
    });

    // Scheduled fargate task to run the scraper daily at 6:00am
    const scheduledFargateTask = new ScheduledFargateTask(this, 'scraper-scheduled-fargate-task', {
      cluster,
      scheduledFargateTaskImageOptions: {
        image: ContainerImage.fromEcrRepository(repository, 'latest'),

        // 4GB RAM
        memoryLimitMiB: 3072,

        // 1 vCPU
        cpu: 512,

        // environment variables to pass to the container
        environment: {
          // dev
          //'APPSYNC_URL': 'https://wdtnkg75gnalzfnf2mgv2jmpce.appsync-api.us-west-2.amazonaws.com/graphql',

          // prod
          'APPSYNC_URL': 'https://6vy4hkcoijbibfyewgixl63pma.appsync-api.us-west-2.amazonaws.com/graphql',
        },

        // secret environment variables to pass to the container
        secrets: {
          'AWS_ACCESS_KEY_ID': Secret.fromSsmParameter(keyIdParameter),
          'AWS_SECRET_KEY': Secret.fromSsmParameter(secretKeyParameter)
        }
      },
      schedule: Schedule.expression('rate(7 days)'),
      platformVersion: FargatePlatformVersion.LATEST,
      subnetSelection: { 
        subnetType: SubnetType.PUBLIC 
      },
    });
  }
}
